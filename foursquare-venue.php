<?php
/*
Plugin Name: Foursquare Venue
Plugin URI: http://sutherlandboswell.com/2010/11/foursquare-venue-wordpress-plugin/
Description: Display your venue's foursquare stats in an easy-to-use widget or with the <code>[venue id=3945]</code> shortcode.
Author: Sutherland Boswell
Author URI: http://sutherlandboswell.com
Version: 3.0.2
License: GPL2
*/
/*  Copyright 2014 Sutherland Boswell  (email : sutherland.boswell@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Define
define( 'FOURSQUARE_VENUE_PATH', dirname(__FILE__) );
define( 'FOURSQUARE_VENUE_VERSION', '3.0.2' );

// Includes
require_once( FOURSQUARE_VENUE_PATH . '/php/class-refactored-settings.php' );
require_once( FOURSQUARE_VENUE_PATH . '/php/widget.php' );

// Plugin Class
class Foursquare_Venue_Plugin {
	
	function __construct() {
		/**
		 * Settings
		 */
		$settings_args = array(
			'file'    => __FILE__,
			'version' => FOURSQUARE_VENUE_VERSION,
			'name'    => 'Foursquare Venue',
			'slug'    => 'foursquare_venue',
			'options' => array(
				'api_key' => array(
					'name'        => 'API Key',
					'description' => '<a href="https://foursquare.com/developers/apps">Register your app</a> and enter your keys below.',
					'fields'      => array(
						'client_id' => array(
							'name'        => 'Client ID',
							'type'        => 'text',
							'default'     => '',
							'description' => ''
						),
						'client_secret' => array(
							'name'        => 'Client Secret',
							'type'        => 'text',
							'default'     => '',
							'description' => ''
						)
					)
				),
				'display' => array(
					'name'        => 'Display',
					'description' => 'These settings control how the widget and <code>[venue]</code> shortcode are displayed.',
					'fields'      => array(
						'photos' => array(
							'name'        => 'Max Photos',
							'type'        => 'text',
							'default'     => 0,
							'description' => ''
						),
						'template_photos' => array(
							'name'        => 'Template Photos',
							'type'        => 'textarea',
							'class'       => 'code',
							'default'     => '<img src="%%photo_prefix%%75x75%%photo_suffix%%">',
                                                        'description' => 'Available placeholders: photo_prefix, photo_suffix'
						),
						'tips' => array(
							'name'        => 'Max Tips',
							'type'        => 'text',
							'default'     => 5,
							'description' => ''
						),
						'template_tips' => array(
							'name'        => 'Template Tips',
							'type'        => 'textarea',
							'class'       => 'code',
							'default'     => '<blockquote><img src="%%user_photo_prefix%%30x30%%user_photo_suffix%%" style="float: left; margin-right: 10px;"><p>%%user_firstname%% am %%date%%: %%text%%</p></blockquote>',
                                                        'description' => 'Available placeholders: user_photo_prefix, user_photo_suffix, user_firstname, user_lastname, date, text'
						),
						'template' => array(
							'name'        => 'Template',
							'type'        => 'textarea',
							'class'       => 'code',
							'default'     => '<p><a href="%%foursquareUrl%%">%%name%%</a><br>%%address%%<br>%%postal_code%% %%city%%<br></p><p>Twitter: <a href="https://twitter.com/%%twitter%%" target="_blank">%%twitter%%</a><br>Facebook: <a href="https://facebook.com/%%facebook%%" target="_blank">https://facebook.com/%%facebook%%</a><br></p><p>%%likes_summary%%<br>%%checkins_count%% check-ins<br>Rating: %%rating%%</p><strong>Fotos:</strong><br> %%photos%%<br><br><hr><strong>Tips (%%tip_count%%):</strong><br> %%tips%%',
							'description' => 'To view available placeholders, open the <a href="#" id="open-help-tab">Help</a> tab in the top right.<table class="foursquare-venue-template-preview-table"><tr><th>Preview</th></tr><tr><td><div id="foursquare-venue-template-preview"></div></td></tr></table>'
						)
					)
				),
			)
		);
		$this->settings = new Refactored_Settings_0_4( $settings_args );
                
		/**
		 * Add scripts and styles to settings page
		 */
		add_action( 'admin_init', array( &$this, 'settings_scripts' ), 20 );
		/**
		 * Add AJAX preview callback for settings page
		 */
		add_action( 'wp_ajax_foursquare_venue_template_preview', array( &$this, 'template_preview_callback' ) );
		/**
		 * Add Help tab
		 */
		add_action( 'load-settings_page_foursquare_venue', array( &$this, 'add_help_tab' ) );
		/**
		 * Register shortcode
		 */
		add_shortcode( 'venue', array( &$this, 'venue_shortcode' ) );
		/**
		 * Delete old settings
		 */
		$this->delete_old_settings();
	}

	/**
	 * Deletes pre-3.0 settings
	 */
	function delete_old_settings() {
		delete_option('foursquare_venue_client_id');
		delete_option('foursquare_venue_client_secret');
		delete_option('foursquare_venue_show_title');
		delete_option('foursquare_venue_stats_title');
		delete_option('foursquare_show_venue_name');
		delete_option('foursquare_show_venue_icon');
		delete_option('foursquare_show_here_now');
		delete_option('foursquare_show_here_now_text');
		delete_option('foursquare_show_total');
		delete_option('foursquare_show_total_text');
		delete_option('foursquare_show_mayor');
		delete_option('foursquare_show_mayor_text');
		delete_option('foursquare_link_mayor');
		delete_option('foursquare_show_mayor_photo');
		delete_option('foursquare_mayor_photo_size');
		delete_option('foursquare_venue_stats_width');
		delete_option('foursquare_venue_stats_align');
	}

	/**
	 * Adds javascript and CSS to the settings page
	 * @return void
	 */
	function settings_scripts() {
		wp_enqueue_script( 'foursquare-venue-settings-js', plugins_url( '/js/settings.js' , __FILE__ ), array( 'jquery' ), FOURSQUARE_VENUE_VERSION );
		wp_enqueue_style( 'foursquare-venue-admin-css', plugins_url( '/css/admin.css', __FILE__ ), false, FOURSQUARE_VENUE_VERSION );
	}

	/**
	 * The venue shortcode's callback function
	 * @param  array  $atts Shortcode attributes
	 * @return string       The shortcode's HTML
	 */
	function venue_shortcode( $atts ) {
		extract(shortcode_atts(array(
			'id' => '',
                        'key' => '',
		), $atts));
		return $this->render_venue( $id, $key );
	}

	/**
	 * Renders a venue's info as an HTML string
	 * @param  string $id The Foursquare venue ID
	 * @return string     An HTML string
	 */
	function render_venue( $id, $key = '' ) {
		$venue = $this->get_venue( $id );
                if ($key) {
                    return $this->fill_key($venue, $key);
                }
                else {
                    $default_tpl = $this->fill_template( $venue, $this->settings->options['display']['template'] );
                    $photos_tpl = $this->fill_photos_template( $venue, $this->settings->options['display']['template_photos'] );
                    $tips_tpl = $this->fill_tips_template( $venue, $this->settings->options['display']['template_tips'] );
                    $default_tpl = str_replace( '%%photos%%', $photos_tpl, $default_tpl );
                    $default_tpl = str_replace( '%%tips%%', $tips_tpl, $default_tpl );
                    return $default_tpl;
                }
	}

	/**
	 * Callback function for the AJAX call that displays a preview of the venue template
	 */
	function template_preview_callback() {
		// Security checks
		if ( !current_user_can( 'manage_options' ) ) die();

		$template = $_POST['template'];
		$venue = $this->get_venue( '4b2cafe3f964a5208bc824e3' );
                
                $default_tpl = $this->fill_template( $venue, $template );
                $photos_tpl = $this->fill_photos_template( $venue, $this->settings->options['display']['template_photos'] );
                $tips_tpl = $this->fill_tips_template( $venue, $this->settings->options['display']['template_tips'] );
		$default_tpl = str_replace( '%%photos%%', $photos_tpl, $default_tpl );
                $default_tpl = str_replace( '%%tips%%', $tips_tpl, $default_tpl );
                echo $default_tpl;
		die();
	}
        
        /**
         * Returns the venue key data
         * @param type $venue
         * @return string     The data of the venue key
         */
        function fill_key( $venue, $key ) {
		if ( is_wp_error( $venue ) ) {
			$result = $venue->get_error_message();
		} else {
                        if ($key == 'photos') {
                            $result = $this->fill_photos_template( $venue, $this->settings->options['display']['template_photos'] );
                        } else if ($key == 'tips'){
                            $result = $this->fill_tips_template( $venue, $this->settings->options['display']['template_tips'] );
                        } else {
                            $replacements = $this->get_replacements( $venue );
                            $result = ( isset( $replacements[$key] ) ? $replacements[$key] : '' );
                        }
		}
                return $result;
        }

	/**
	 * Fills a template with venue data
	 * @param  object $venue    The venue data retrieved from Foursquare
	 * @param  string $template A template containing placeholders to be filled
	 * @return string           The HTML containing filled in data
	 */
	function fill_template( $venue, $template ) {                
		if ( is_wp_error( $venue ) ) {
			$result = $venue->get_error_message();
		} else {
			$result = $template;
			$replacements = $this->get_replacements( $venue );
			foreach ( $replacements as $key => $value) {
				$result = str_replace( '%%' . $key . '%%', $value, $result );
			}
		}
                return $result;
	}
        
	function fill_photos_template( $venue, $template ) {                
		if ( is_wp_error( $venue ) ) {
			$result = $venue->get_error_message();
		} else {
			$result = '';
			$photos = $this->get_photos( $venue );
			foreach ( $photos as $photo ) {
                                $row = $template;
                                foreach ( $photo as $key => $value ) {
                                    $row = str_replace( '%%' . $key . '%%', $value, $row );
                                }
                                $result .= $row;
			}
		}
                return $result;
	}
        
	function fill_tips_template( $venue, $template ) {                
		if ( is_wp_error( $venue ) ) {
			$result = $venue->get_error_message();
		} else {
			$result = '';
			$tips = $this->get_tips( $venue );                        
			foreach ( $tips as $tip) {
                            $row = $template;
                            foreach ( $tip as $key => $value ) {                                
				$row = str_replace( '%%' . $key . '%%', $value, $row );
                            }
                            $result .= $row;
			}
		}
                return $result;
	}

	/**
	 * Gets an array of values available for use in venue templates
	 * @param  object $venue The venue data retrieved from foursquare
	 * @return array         An array of keys available for replacement with real data as the value
	 */
	function get_replacements( $venue ) {
                //print_r($venue);
		$replacements = array(
			'id'               => ( isset( $venue->id ) ? $venue->id : '' ),
			'name'             => ( isset( $venue->name ) ? $venue->name : '' ),
                        // contact 
			'phone'            => ( isset( $venue->contact->phone ) ? $venue->contact->phone : '' ),
			'formattedPhone'   => ( isset( $venue->contact->formattedPhone ) ? $venue->contact->formattedPhone : '' ),
                        'facebook'         => ( isset( $venue->contact->facebook ) ? $venue->contact->facebook : '' ),
                        'facebookName'     => ( isset( $venue->contact->facebookName ) ? $venue->contact->facebookName : '' ),
			'twitter'          => ( isset( $venue->contact->twitter ) ? $venue->contact->twitter : '' ),
                        // location
			'address'          => ( isset( $venue->location->address ) ? $venue->location->address : '' ),
			'cross_street'     => ( isset( $venue->location->crossStreet ) ? $venue->location->crossStreet : '' ),
			'lat'              => ( isset( $venue->location->lat ) ? $venue->location->lat : '' ),
			'lng'              => ( isset( $venue->location->lng ) ? $venue->location->lng : '' ),
			'postal_code'      => ( isset( $venue->location->postalCode ) ? $venue->location->postalCode : '' ),
			'cc'               => ( isset( $venue->location->cc ) ? $venue->location->cc : '' ),
			'city'             => ( isset( $venue->location->city ) ? $venue->location->city : '' ),
			'state'            => ( isset( $venue->location->state ) ? $venue->location->state : '' ),
			'country'          => ( isset( $venue->location->country ) ? $venue->location->country : '' ),
                        
			'foursquareUrl'    => ( isset( $venue->canonicalUrl ) ? $venue->canonicalUrl : '' ),
			'short_url'        => ( isset( $venue->shortUrl ) ? $venue->shortUrl : '' ),
			'website_url'      => ( isset( $venue->url ) ? $venue->url : '' ),
                        // stats
			'checkins_count'   => ( isset( $venue->stats->checkinsCount ) ? $venue->stats->checkinsCount : '' ),
			'users_count'      => ( isset( $venue->stats->usersCount ) ? $venue->stats->usersCount : '' ),
			'tip_count'        => ( isset( $venue->stats->tipCount ) ? $venue->stats->tipCount : '' ),
                        // likes
			'likes_count'      => ( isset( $venue->likes->count ) ? $venue->likes->count : '' ),
			'likes_summary'    => ( isset( $venue->likes->summary ) ? $venue->likes->summary : '' ),
                        // rating
			'rating'           => ( isset( $venue->rating ) ? $venue->rating : '' ),
			'ratingSignals'    => ( isset( $venue->ratingSignals ) ? $venue->ratingSignals : '' ),
                        // hereNow
			'here_now_count'   => ( isset( $venue->hereNow->count ) ? $venue->hereNow->count : '' ),
			'here_now_summary' => ( isset( $venue->hereNow->summary ) ? $venue->hereNow->summary : '' ),
                        // description
			'description'      => ( isset( $venue->description ) ? $venue->description : '' ),
                        // mayor
			'mayor_count'      => ( isset( $venue->mayor->count ) ? $venue->mayor->count : '' ),
			'mayor_id'         => ( isset( $venue->mayor->user->id ) ? $venue->mayor->user->id : '' ),
			'mayor_url'        => ( isset( $venue->mayor->user->id ) ? 'https://foursquare.com/user/' . $venue->mayor->user->id : '' ),
			'mayor_first_name' => ( isset( $venue->mayor->user->firstName ) ? $venue->mayor->user->firstName : '' ),
			'mayor_last_name'  => ( isset( $venue->mayor->user->lastName ) ? $venue->mayor->user->lastName : '' ),
			'mayor_gender'     => ( isset( $venue->mayor->user->gender ) ? $venue->mayor->user->gender : '' ),
			'mayor_photo'      => ( isset( $venue->mayor->user->photo->prefix ) ? $venue->mayor->user->photo->prefix . '100x100'. $venue->mayor->user->photo->suffix : '' ),
                        'bestPhoto'        => ( isset( $venue->bestPhoto->prefix ) ? $venue->bestPhoto->prefix . '100x100'. $venue->bestPhoto->suffix : '' )
		);
                
		return apply_filters( 'foursquare_venue/replacements', $replacements, $venue );
	}
        
        function get_photos( $venue ) {
                // add photos
                $max_photos = count($venue->photos->groups[0]->items);
                $photos_count = $this->settings->options['display']['photos'] > 1 ? $this->settings->options['display']['photos'] : $max_photos;
                if ($photos_count > $max_photos) {
                    $photos_count = $max_photos;
                }
                $photos = array();
                for ($i = 0; $i < $photos_count; ++$i) {
                    $photo = $venue->photos->groups[0]->items[$i];
                    $photos[$i]['photo_prefix'] = $photo->prefix;
                    $photos[$i]['photo_suffix'] = $photo->suffix;
                }
                return $photos;
        }        
        
        function get_tips( $venue ) {
                // add tips
 
                // sort by createdAt
                usort($venue->tips->groups[0]->items, function($a, $b) {
                    return $b->createdAt - $a->createdAt;
                });                
                // create return array
                $max_tips = count($venue->tips->groups[0]->items);
                $tips_count = $this->settings->options['display']['tips'] > 1 ? $this->settings->options['display']['tips'] : $max_tips;
                if ($tips_count > $max_tips)  {
                    $tips_count = $max_tips;
                }
                $tips = array();
                for ($i = 0; $i < $tips_count; ++$i) {
                    $tip = $venue->tips->groups[0]->items[$i];
                    $tips[$i]['user_photo_prefix'] = $tip->user->photo->prefix;
                    $tips[$i]['user_photo_suffix'] = $tip->user->photo->suffix;
                    $tips[$i]['user_firstname'] = $tip->user->firstName;
                    $tips[$i]['user_lastname'] = $tip->user->lastName;
                    $tips[$i]['text'] = $tip->text;
                    $tips[$i]['date'] = date('d.m.Y', $tip->createdAt);
                }                
                return $tips;
        }

	/**
	 * Cache the venue data using WP transients
	 * @param  string  $id   The Foursquare venue ID
	 * @param  object  $data The venue data
	 * @return boolean       True if data was saved, false otherwise
	 */
	function save_venue_cache( $id, $data ) {
		// The name our transient will be stored as
		$transient_name = 'foursquare_venue_' . $id;
		// Serialize the data, then base 64 encode it to handle emoji and other characters
		$data = base64_encode( serialize( $data ) );
		// The number of seconds to store the transient
		$expiration = 15 * 60;
		// Save it
		return set_transient( $transient_name, $data, $expiration );
	}

	/**
	 * Get the venue data from cache if available
	 * @param  string $id The Foursquare Venue ID
	 * @return mixed      A Foursquare data object or false if no cache
	 */
	function get_venue_cache( $id ) {
		// The name our transient is stored as
		$transient_name = 'foursquare_venue_' . $id;
		// Pull the data
		$data = get_transient( $transient_name );
		// If we have data, and it isn't already an object, decode and unserialize it
		if ( $data && !is_object( $data ) ) {
			$data = unserialize( base64_decode( $data ) );
		}
		// Give the data
		return $data;
	}

	/**
	 * Get a venue's data from the Foursquare API or local cache
	 * @param  string $id The Foursquare venue ID
	 * @return object     The venue data
	 */
	function get_venue( $id ) {
		// Check for cached data
		$data = $this->get_venue_cache( $id );
		// If we don't have data, make an API request
		if ( $data === false ) {
			$request_url = 'https://api.foursquare.com/v2/venues/' . $id . '?client_id=' . $this->settings->options['api_key']['client_id'] . '&client_secret=' . $this->settings->options['api_key']['client_secret'] . '&v=20140702';
			$response = wp_remote_get( $request_url, array( 'sslverify' => false ) );
			if( is_wp_error( $response ) ) {
				$data = $response;
			} else {
				$data = json_decode( $response['body'] );
				if ( $data->meta->code != 200 ) {
					$data = new WP_Error( 'foursquare_api_error', $data->meta->errorDetail );
				} else {
					$data = $data->response->venue;
					$this->save_venue_cache( $id, $data );
				}
			}
		}
		// Give up the data
		return $data;
	}

	/**
	 * This callback adds the help tab's content to the settings page
	 */
	function add_help_tab() {
		$screen = get_current_screen();

		$venue = $this->get_venue( '3fd66200f964a520d6f01ee3' );
		if ( is_wp_error( $venue ) ) {
			$table = '<p><strong>' . __( 'Error:' ) . '</strong> ' . $venue->get_error_message() . '</p>';
		} else {
			$table = '<table class="foursquare-venue-replacements"><thead><tr><th>' . __( 'Placeholder' ) . '</th><td>' . __( 'Example Value' ) . '</td></tr></thead><tbody>';
			$replacements = $this->get_replacements( $venue );
			foreach ( $replacements as $key => $value ) {
				$table .= '<tr><th>%%' . $key . '%%</th><td>' . $value . '</td></tr>';
			}
			$table .= '</tbody></table>';
		}

		// Add placeholder help tab
		$screen->add_help_tab( array(
			'id'	=> 'foursquare_venue_placeholder_help_tab',
			'title'	=> __('Venue Placeholders'),
			'content'	=> '<h2>' . __( 'Venue Placeholders' ) . '</h2><p>' . __( 'These placeholders will be replaced with the appropriate data in your display template.' ) . '</p>' . $table,
		) );
		// Add shortcode help tab
		$screen->add_help_tab( array(
			'id'	=> 'foursquare_venue_shortcode_help_tab',
			'title'	=> __('Venue Shortcode'),
			'content'	=> '<h2>' . __( 'Venue Shortcode' ) . '</h2><p>' . __( 'The <code>[venue]</code> shortcode can be used to display venue information and stats from Foursquare.' ) . '</p><p>' . __( 'To use the shortcode you will need the venue\'s Foursquare ID. The ID is found at the end of the venue\'s Foursquare URL. For example, The White House\'s venue URL is <code>https://foursquare.com/v/the-white-house/3fd66200f964a520d6f01ee3</code>, which means the ID is <code>3fd66200f964a520d6f01ee3</code>.' ) . '</p><p>' . __( 'Once you have the venue ID, you can insert the shortcode with the venue ID included. Example: <code>[venue id="3fd66200f964a520d6f01ee3"]</code>' ) . '</p>'  . __( 'If you only want to insert a specific venue key you can add the value by adding a key attribute. Example: <code>[venue id="3fd66200f964a520d6f01ee3" key="address"]</code>' ) . '</p>'
		) );

		$screen->set_help_sidebar( '<p>' . __( 'For support, please visit the <a href="http://wordpress.org/support/plugin/foursquare-venue">WordPress forums</a>.' ) . '</p>' );
	}

}

$foursquare_venue_plugin = new Foursquare_Venue_Plugin();

?>