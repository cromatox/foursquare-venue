<?php
/*  Copyright 2014 Sutherland Boswell  (email : sutherland.boswell@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

class Foursquare_Venue_Widget extends WP_Widget 
{

	function Foursquare_Venue_Widget() 
	{
		$widget_ops = array(
			'classname' => 'foursquare_venue_widget',
			'description' => 'Foursquare Venue'
		);
		$this->WP_Widget( 'foursquare_venue', 'Foursquare Venue', $widget_ops );
	}

	function widget( $args, $instance ) 
	{
		global $foursquare_venue_plugin;

		extract( $args );
		
		$id = str_replace( 'https://foursquare.com/venue/', '', $instance['venue_id'] );
		$id = str_replace( 'http://foursquare.com/venue/', '', $id );

		echo $before_widget;
		$title = strip_tags( $instance['title'] );
		echo $before_title . $title . $after_title;

		echo $foursquare_venue_plugin->render_venue( $id );

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['venue_id'] = trim(strip_tags($new_instance['venue_id']));

		return $instance;
	}

	function form( $instance ) 
	{
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Foursquare', 'venue_id' => 3945 ) );
		$title = strip_tags($instance['title']);
		$venue_id = strip_tags($instance['venue_id']);
		echo '<p><label for="' . $this->get_field_id( 'title' ) . ' ">Title: <input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $title ) . '" /></label></p>';
		echo '<p><label for="' . $this->get_field_id( 'venue_id' ) . '">Venue ID: <input class="widefat" id="' . $this->get_field_id( 'venue_id' ) . '" name="' . $this->get_field_name( 'venue_id' ) . '" type="text" value="' . esc_attr( $venue_id ) . '" /></label></p>';
	}

}

add_action('widgets_init', 'RegisterFoursquareVenueWidget');

function RegisterFoursquareVenueWidget() {
	register_widget('Foursquare_Venue_Widget');
}