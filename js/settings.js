jQuery(document).ready(function($) {

	function update_foursquare_venue_template_preview() {
		var data = {
			action: 'foursquare_venue_template_preview',
			template: $("textarea#foursquare_venue-template").val()
		};
		$.post(ajaxurl, data, function(response) {
			$('#foursquare-venue-template-preview').html(response);
		});
	}

	$('#open-help-tab').on('click', function() {
		$('#contextual-help-link').click();
	});

	$("textarea#foursquare_venue-template").on('change keyup paste ready', function() {
		update_foursquare_venue_template_preview();
	});

	update_foursquare_venue_template_preview();

});